package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysFreeAuth;
import com.example.tongyao.system.entity.SysInterface;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单接口表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysInterfaceService extends IService<SysInterface> {

    List<SysInterface> interfacePage(Map<String, Object> params);

}
