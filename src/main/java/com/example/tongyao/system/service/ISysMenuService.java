package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysMenuService extends IService<SysMenu> {

    String getMenuInfoByRoleIdDeptId(String roleId, String deptId);
}
