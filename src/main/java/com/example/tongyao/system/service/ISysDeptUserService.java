package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysDeptUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门用户数据范围表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface ISysDeptUserService extends IService<SysDeptUser> {

}
