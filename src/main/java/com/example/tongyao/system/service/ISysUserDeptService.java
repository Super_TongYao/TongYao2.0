package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.system.entity.SysUserDept;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户部门表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysUserDeptService extends IService<SysUserDept> {

    Map<String,Object> getUserDeptInfoByUserId(String userId);

}
