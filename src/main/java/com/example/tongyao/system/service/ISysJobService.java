package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysJob;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 岗位表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface ISysJobService extends IService<SysJob> {

}
