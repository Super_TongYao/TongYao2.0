package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysJob;
import com.example.tongyao.system.mapper.SysJobMapper;
import com.example.tongyao.system.service.ISysJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Service
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements ISysJobService {

}
