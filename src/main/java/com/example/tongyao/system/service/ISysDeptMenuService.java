package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysDeptMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门菜单表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysDeptMenuService extends IService<SysDeptMenu> {

}
