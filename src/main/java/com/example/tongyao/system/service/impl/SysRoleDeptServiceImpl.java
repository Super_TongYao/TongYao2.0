package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysRoleDept;
import com.example.tongyao.system.mapper.SysRoleDeptMapper;
import com.example.tongyao.system.service.ISysRoleDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色部门数据范围表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {

}
