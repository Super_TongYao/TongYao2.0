package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysDeptUser;
import com.example.tongyao.system.mapper.SysDeptUserMapper;
import com.example.tongyao.system.service.ISysDeptUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门用户数据范围表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Service
public class SysDeptUserServiceImpl extends ServiceImpl<SysDeptUserMapper, SysDeptUser> implements ISysDeptUserService {

}
