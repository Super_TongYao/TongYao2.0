package com.example.tongyao.system.service;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CommonService {

    public Map<String,Object> getOne(String field,String table,String where);

    public List<Map<String,Object>> getMore(String field, String table, String where);

    public int getCount(String table,String where);
}
