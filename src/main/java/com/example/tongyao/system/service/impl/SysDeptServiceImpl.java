package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysDept;
import com.example.tongyao.system.mapper.SysDeptMapper;
import com.example.tongyao.system.service.ISysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Resource
    private SysDeptMapper sysDeptMapper;

    @Override
    public List<SysDept> deptPage(Map<String, Object> params) {
        return sysDeptMapper.deptPage(params);
    }
}
