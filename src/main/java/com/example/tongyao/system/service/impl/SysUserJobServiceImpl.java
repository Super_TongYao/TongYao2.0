package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysUserJob;
import com.example.tongyao.system.mapper.SysUserJobMapper;
import com.example.tongyao.system.service.ISysUserJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 用户岗位表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Service
public class SysUserJobServiceImpl extends ServiceImpl<SysUserJobMapper, SysUserJob> implements ISysUserJobService {

    @Resource
    private SysUserJobMapper sysUserJobMapper;

    @Override
    public Map<String, Object> getUserJobInfoByUserId(String userId) {
        return sysUserJobMapper.getUserJobInfoByUserId(userId);
    }
}
