package com.example.tongyao.system.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.entity.SysUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MyUserDetailsService implements UserDetailsService {

    @Resource
    private ISysUserService iSysUserService;

    @Resource
    private ISysUserRoleService iSysUserRoleService;

    @Resource
    private ISysUserDeptService iSysUserDeptService;

    @Resource
    private ISysUserJobService iSysUserJobService;

    @Resource
    private ISysMenuService iSysMenuService;

    @Resource
    private CommonService commonService;

    @Value("${login.auth.type}")
    String loginAuthType;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        LambdaQueryWrapper<SysUser> sysUserLambdaQueryWrapper = new LambdaQueryWrapper<>();
        /*sysUserLambdaQueryWrapper.select(
                SysUser::getUserId,
                SysUser::getUsername,
                SysUser::getPassword,
                SysUser::isEnabled,
                SysUser::isAccountNonExpired,
                SysUser::isAccountNonLocked,
                SysUser::isCredentialsNonExpired);*/
        sysUserLambdaQueryWrapper.eq(SysUser::getUsername,userName);
        sysUserLambdaQueryWrapper.eq(SysUser::getDelFlag,"1");
        sysUserLambdaQueryWrapper.eq(SysUser::getIdType,"2");
        List<SysUser> sysUserList = iSysUserService.list(sysUserLambdaQueryWrapper);

        if(sysUserList.size() == 1){


            SysUser sysUser = sysUserList.get(0);
            //再根据用户id查询相应多个角色id
            Map<String,Object> mapRole = iSysUserRoleService.getUserRoleInfoByUserId(sysUser.getUserId());
            String roleId = mapRole.get("role_id").toString();
            //判断该用户是否有角色
            if(mapRole == null || roleId == null || roleId.equals("")){
                return sysUser;
            }

            sysUser.setRoleId(roleId);
            sysUser.setRoleName(mapRole.get("role_name").toString());

            /*数据权限范围开始*/
            String[] roleIds = roleId.split(",");
            String whereIn = "";
            for (String str : roleIds) {
                whereIn = whereIn + "'" + str + "',";
            }
            whereIn = whereIn.substring(0,whereIn.length()-1);

            List<Map<String,Object>> mapList = commonService.getMore(
                    "distinct data_scope",
                    "sys_role",
                    "role_id in ("+whereIn+") and del_flag = 1"
            );
            List dataScopeList = new ArrayList();
            for(Map map : mapList){
                dataScopeList.add(map.get("data_scope").toString());
            }
            sysUser.setDataScope(dataScopeList);
            /*数据权限范围结束*/

            //再根据用户id查询相应多个部门id
            Map<String,Object> mapDept = iSysUserDeptService.getUserDeptInfoByUserId(sysUser.getUserId());
            sysUser.setDeptId(mapDept.get("dept_id").toString());
            sysUser.setDeptName(mapDept.get("dept_name").toString());

            //再根据岗位id查询相应多个岗位id
            Map<String,Object> mapJob = iSysUserJobService.getUserJobInfoByUserId(sysUser.getUserId());
            sysUser.setJobId(mapJob.get("job_id").toString());
            sysUser.setJobName(mapJob.get("job_name").toString());

            if(loginAuthType.equals("to_menu")){
                //查询相应菜单权限字段
                String permission = iSysMenuService.getMenuInfoByRoleIdDeptId(
                        mapRole.get("role_id").toString(),
                        mapDept.get("dept_id").toString()
                );
                sysUser.setRole(permission);
            }
            int i = 0;
            return sysUser;
        }else{
            return null;
        }

    }

}