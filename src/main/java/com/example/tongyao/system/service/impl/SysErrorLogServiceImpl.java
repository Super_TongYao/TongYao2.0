package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysErrorLog;
import com.example.tongyao.system.mapper.SysErrorLogMapper;
import com.example.tongyao.system.service.ISysErrorLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志操作表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-11-16
 */
@Service
public class SysErrorLogServiceImpl extends ServiceImpl<SysErrorLogMapper, SysErrorLog> implements ISysErrorLogService {

}
