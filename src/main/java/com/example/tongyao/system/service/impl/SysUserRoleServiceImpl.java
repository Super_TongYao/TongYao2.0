package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysUserRole;
import com.example.tongyao.system.mapper.SysUserRoleMapper;
import com.example.tongyao.system.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public Map<String, Object> getUserRoleInfoByUserId(String userId) {
        return sysUserRoleMapper.getUserRoleInfoByUserId(userId);
    }
}
