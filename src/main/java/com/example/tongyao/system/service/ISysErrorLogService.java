package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysErrorLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 日志操作表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-11-16
 */
public interface ISysErrorLogService extends IService<SysErrorLog> {

}
