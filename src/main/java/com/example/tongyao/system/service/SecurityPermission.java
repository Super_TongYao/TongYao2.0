package com.example.tongyao.system.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * 返回权限验证的接口
 */
public interface SecurityPermission {

    boolean addPermission(HttpServletRequest request, Authentication authentication);
}
