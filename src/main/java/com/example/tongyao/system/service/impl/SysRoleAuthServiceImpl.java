package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysRoleAuth;
import com.example.tongyao.system.mapper.SysRoleAuthMapper;
import com.example.tongyao.system.service.ISysRoleAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 角色接口 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysRoleAuthServiceImpl extends ServiceImpl<SysRoleAuthMapper, SysRoleAuth> implements ISysRoleAuthService {

    @Resource
    private SysRoleAuthMapper sysRoleAuthMapper;

    @Override
    public List<String> getAuthMenuInterfaceAddressByRoleId(String roleId) {
        return sysRoleAuthMapper.getAuthMenuInterfaceAddressByRoleId(roleId);
    }
}
