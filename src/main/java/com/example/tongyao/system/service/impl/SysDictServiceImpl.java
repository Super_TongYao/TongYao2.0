package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysDict;
import com.example.tongyao.system.mapper.SysDictMapper;
import com.example.tongyao.system.service.ISysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统字典表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

}
