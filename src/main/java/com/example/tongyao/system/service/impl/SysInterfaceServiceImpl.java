package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysFreeAuth;
import com.example.tongyao.system.entity.SysInterface;
import com.example.tongyao.system.mapper.SysInterfaceMapper;
import com.example.tongyao.system.service.ISysInterfaceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单接口表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysInterfaceServiceImpl extends ServiceImpl<SysInterfaceMapper, SysInterface> implements ISysInterfaceService {

    @Resource
    private SysInterfaceMapper sysInterfaceMapper;

    @Override
    public List<SysInterface> interfacePage(Map<String, Object> params) {
        return sysInterfaceMapper.interfacePage(params);
    }
}
