package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysConfigService extends IService<SysConfig> {

    SysConfig getConfigByKey(String configKey);
}
