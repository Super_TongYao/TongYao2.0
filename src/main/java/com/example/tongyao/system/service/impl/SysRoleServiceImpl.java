package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.entity.SysRole;
import com.example.tongyao.system.mapper.SysRoleMapper;
import com.example.tongyao.system.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Override
    public List<SysMenu> getRoleInterfaceByRoleId(String roleId) {
        return sysRoleMapper.getRoleInterfaceByRoleId(roleId);
    }
}
