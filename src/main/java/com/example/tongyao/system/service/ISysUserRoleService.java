package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

    Map<String,Object> getUserRoleInfoByUserId(String userId);
}
