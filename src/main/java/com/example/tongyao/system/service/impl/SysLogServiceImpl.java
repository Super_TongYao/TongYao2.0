package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysLog;
import com.example.tongyao.system.mapper.SysLogMapper;
import com.example.tongyao.system.service.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志操作表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-11-07
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

}
