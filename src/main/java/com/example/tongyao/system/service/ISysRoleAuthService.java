package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysRoleAuth;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色接口 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysRoleAuthService extends IService<SysRoleAuth> {

    public List<String> getAuthMenuInterfaceAddressByRoleId(String roleId);
}
