package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 日志操作表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-11-07
 */
public interface ISysLogService extends IService<SysLog> {

}
