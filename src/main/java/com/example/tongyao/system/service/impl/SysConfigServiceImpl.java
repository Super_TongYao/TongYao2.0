package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysConfig;
import com.example.tongyao.system.mapper.SysConfigMapper;
import com.example.tongyao.system.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Resource
    private SysConfigMapper sysConfigMapper;

    @Override
    public SysConfig getConfigByKey(String configKey) {
        return sysConfigMapper.getConfigByKey(configKey);
    }
}
