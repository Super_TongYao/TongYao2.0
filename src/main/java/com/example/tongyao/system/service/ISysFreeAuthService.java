package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysFreeAuth;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限路径表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysFreeAuthService extends IService<SysFreeAuth> {

    List<SysFreeAuth> freeAuthPage(Map<String, Object> params);
}
