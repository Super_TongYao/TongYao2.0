package com.example.tongyao.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.system.mapper.SysUserMapper;
import com.example.tongyao.system.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-15
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Resource
    private SysUserMapper sysUserMapper;



    @Override
    public boolean deleteUserByUserId(String userId) {
        return sysUserMapper.deleteUserByUserId(userId);
    }

    @Override
    public boolean resetPasswordByUserId(String userId,String password) {
        return sysUserMapper.resetPasswordByUserId(userId,password);
    }

    /*@Override
    public List<SysUser> userPage(Map<String, Object> params) {
        return sysUserMapper.userPage(params);
    }
    @Override
    public int userPageCount(Map<String, Object> params) {
        return sysUserMapper.userPageCount(params);
    }*/

    @Override
    public IPage<SysUser> userPage(Page<Map<String, Object>> page, Map<String, Object> params) {
        return sysUserMapper.userPage(page,params);
    }
}
