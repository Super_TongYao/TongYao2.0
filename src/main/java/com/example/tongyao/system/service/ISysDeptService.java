package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysDeptService extends IService<SysDept> {

    List<SysDept> deptPage(Map<String, Object> params);
}
