package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysDeptMenu;
import com.example.tongyao.system.mapper.SysDeptMenuMapper;
import com.example.tongyao.system.service.ISysDeptMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门菜单表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysDeptMenuServiceImpl extends ServiceImpl<SysDeptMenuMapper, SysDeptMenu> implements ISysDeptMenuService {

}
