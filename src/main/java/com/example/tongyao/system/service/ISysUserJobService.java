package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysUserJob;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户岗位表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface ISysUserJobService extends IService<SysUserJob> {

    Map<String,Object> getUserJobInfoByUserId(String userId);
}
