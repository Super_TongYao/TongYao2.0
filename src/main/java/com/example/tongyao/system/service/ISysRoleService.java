package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysRoleService extends IService<SysRole> {

    public List<SysMenu> getRoleInterfaceByRoleId(String roleId);
}
