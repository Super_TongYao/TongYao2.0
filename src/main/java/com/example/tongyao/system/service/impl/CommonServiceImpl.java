package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.mapper.CommonMapper;
import com.example.tongyao.system.service.CommonService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class CommonServiceImpl implements CommonService {

    @Resource
    private CommonMapper commonMapper;

    @Override
    public Map<String, Object> getOne(String field, String table, String where) {
        return commonMapper.getOne(field,table,where);
    }
    @Override
    public List<Map<String, Object>> getMore(String field, String table, String where) {
        return commonMapper.getMore(field,table,where);
    }
    @Override
    public int getCount(String table, String where) {
        return commonMapper.getCount(table,where);
    }
}
