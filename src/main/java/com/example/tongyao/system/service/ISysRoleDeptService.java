package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysRoleDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色部门数据范围表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {

}
