package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.mapper.SysMenuMapper;
import com.example.tongyao.system.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {


    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public String getMenuInfoByRoleIdDeptId(String role_id, String dept_id) {
        return sysMenuMapper.getMenuInfoByRoleIdDeptId(role_id,dept_id);
    }
}
