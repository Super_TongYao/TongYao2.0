package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统字典表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface ISysDictService extends IService<SysDict> {

}
