package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysUserDept;
import com.example.tongyao.system.mapper.SysUserDeptMapper;
import com.example.tongyao.system.service.ISysUserDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 用户部门表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysUserDeptServiceImpl extends ServiceImpl<SysUserDeptMapper, SysUserDept> implements ISysUserDeptService {

    @Resource
    private SysUserDeptMapper sysUserDeptMapper;

    @Override
    public Map<String, Object> getUserDeptInfoByUserId(String userId) {
        return sysUserDeptMapper.getUserDeptInfoByUserId(userId);
    }
}
