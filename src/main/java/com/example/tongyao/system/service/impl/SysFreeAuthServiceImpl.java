package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysFreeAuth;
import com.example.tongyao.system.mapper.SysFreeAuthMapper;
import com.example.tongyao.system.service.ISysFreeAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限路径表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Service
public class SysFreeAuthServiceImpl extends ServiceImpl<SysFreeAuthMapper, SysFreeAuth> implements ISysFreeAuthService {

    @Resource
    private SysFreeAuthMapper sysFreeAuthMapper;

    @Override
    public List<SysFreeAuth> freeAuthPage(Map<String, Object> params) {
        return sysFreeAuthMapper.freeAuthPage(params);
    }
}
