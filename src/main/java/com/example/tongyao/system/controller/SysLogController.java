package com.example.tongyao.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 日志操作表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2021-11-07
 */
@RestController
@RequestMapping("/system/sys-log")
public class SysLogController {

    //少一个查询接口

}
