package com.example.tongyao.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 部门用户数据范围表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@RestController
@RequestMapping("/system/sys-dept-user")
public class SysDeptUserController {

}
