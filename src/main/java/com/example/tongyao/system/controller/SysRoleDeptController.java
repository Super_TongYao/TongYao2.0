package com.example.tongyao.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色部门数据范围表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@RestController
@RequestMapping("/system/sys-role-dept")
public class SysRoleDeptController {

}
