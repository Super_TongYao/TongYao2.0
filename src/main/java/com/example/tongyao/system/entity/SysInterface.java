package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 菜单接口表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysInterface对象", description="菜单接口表")
public class SysInterface extends SuperEntity {

    @ApiModelProperty(value = "接口编号")
    @TableId(value ="interface_id", type = IdType.ASSIGN_UUID)
    private String interfaceId;

    @ApiModelProperty(value = "菜单编号",required = true)
    @NotBlank(message = "菜单编号不能为空！")
    @TableId(value ="menu_id")
    private String menuId;

    @ApiModelProperty(value = "接口名称",required = true)
    @NotBlank(message = "接口名称不能为空！")
    @TableId(value ="interface_name")
    private String interfaceName;

    @ApiModelProperty(value = "接口描述",required = true)
    @NotBlank(message = "接口描述不能为空！")
    @TableId(value ="interface_desc")
    private String interfaceDesc;

    @ApiModelProperty(value = "接口路径",required = true)
    @NotBlank(message = "接口路径不能为空！")
    @TableId(value ="interface_address")
    private String interfaceAddress;

    @ApiModelProperty(value = "接口类型",required = true)
    @NotBlank(message = "接口类型不能为空！")
    @TableId(value ="interface_type")
    private String interfaceType;

    @ApiModelProperty(value = "接口状态（true为启用，false为禁用）",hidden = true)
    @TableId(value ="interface_enabled")
    private boolean interfaceEnabled;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;


}
