package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysRoleAuth对象", description="角色接口")
public class SysRoleAuth extends SuperEntity {

    @ApiModelProperty(value = "角色接口编号")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="role_auth_path_id", type = IdType.ASSIGN_UUID)
    private String roleAuthPathId;

    @ApiModelProperty(value = "接口编号")
    @TableId(value ="interface_id")
    private String interfaceId;

    @ApiModelProperty(value = "角色编号")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="role_id")
    private String roleId;


}
