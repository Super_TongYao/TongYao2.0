package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 权限路径表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysFreeAuth对象", description="权限路径表")
public class SysFreeAuth extends SuperEntity {

    @ApiModelProperty(value = "免身份认证路径编号")
    @TableId(value ="free_auth_id", type = IdType.ASSIGN_UUID)
    private String freeAuthId;

    @ApiModelProperty(value = "路径地址")
    @NotBlank(message = "路径地址不能为空！")
    @TableId(value ="path_address")
    private String pathAddress;

    @ApiModelProperty(value = "路径描述")
    @TableId(value ="path_desc")
    private String pathDesc;

    @ApiModelProperty(value = "路径类型（免token还是免登录）")
    @NotBlank(message = "路径类型不能为空！")
    @TableId(value ="path_type")
    private String pathType;

    @ApiModelProperty(value = "接口状态（1为未删除，0为已删除）",hidden = true)
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="path_enabled")
    private boolean pathEnabled;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private int sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;


}
