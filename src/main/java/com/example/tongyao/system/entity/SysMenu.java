package com.example.tongyao.system.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysMenu对象", description="菜单表")
public class SysMenu extends SuperEntity {

    @ApiModelProperty(value = "菜单编号")
    @TableId(value ="menu_id", type = IdType.ASSIGN_UUID)
    private String menuId;

    @ApiModelProperty(value = "父级编号",required = true)
    @NotBlank(message = "父级编号不能为空！")
    @TableId(value ="parent_id")
    private String parentId;

    @ApiModelProperty(value = "菜单名称",required = true)
    @NotBlank(message = "菜单名称不能为空！")
    @TableId(value ="menu_name")
    private String menuName;

    @ApiModelProperty(value = "菜单类型",required = true)
    @NotBlank(message = "菜单类型不能为空！")
    @TableId(value ="menu_type")
    private String menuType;

    @ApiModelProperty(value = "菜单权限")
    @TableId(value ="permission")
    private String permission;

    @ApiModelProperty(value = "菜单路径",required = true)
    @NotBlank(message = "菜单路径不能为空！")
    @TableId(value ="menu_path")
    private String menuPath;

    @ApiModelProperty(value = "菜单图标",required = true)
    @NotBlank(message = "菜单图标不能为空！")
    @TableId(value ="menu_icon")
    private String menuIcon;

    @ApiModelProperty(value = "打开方式",required = true)
    @NotBlank(message = "打开方式不能为空！")
    @TableId(value ="open_type")
    private String openType;

    @ApiModelProperty(value = "菜单状态（true为启用，false为禁用）",hidden = true)
    @TableId(value ="menu_enabled")
    private boolean menuEnabled;

    @ApiModelProperty(value = "面包屑",hidden = true)
    @TableId(value ="crumbs")
    private String crumbs;

    @ApiModelProperty(value = "菜单排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "子节点",hidden = true)
    private List<SysMenu> children;
}
