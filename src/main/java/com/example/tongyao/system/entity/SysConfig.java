package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 系统配置表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysConfig对象", description="系统配置表")
public class SysConfig extends SuperEntity {

    @ApiModelProperty(value = "系统配置编号")
    @TableId(value ="config_id", type = IdType.ASSIGN_UUID)
    private String configId;

    @ApiModelProperty(value = "父级",required = true)
    @NotBlank(message = "父级不能为空！")
    @TableId(value ="parent_id")
    private String parentId;

    @ApiModelProperty(value = "配置名称",required = true)
    @NotBlank(message = "配置名称不能为空！")
    @TableId(value ="config_name")
    private String configName;

    @ApiModelProperty(value = "系统配置键",required = true)
    @NotBlank(message = "系统配置键不能为空！")
    @TableId(value ="config_key")
    private String configKey;

    @ApiModelProperty(value = "系统配置值",required = true)
    @NotBlank(message = "系统配置值不能为空！")
    @TableId(value ="config_value")
    private String configValue;

    @ApiModelProperty(value = "配置状态",hidden = true)
    @TableId(value ="config_enabled")
    private boolean configEnabled;

    @ApiModelProperty(value = "是否是系统内置（1为是，0为否）",required = true)
    @NotBlank(message = "是否是系统内置不能为空！")
    @TableId(value ="system_config")
    private Integer systemConfig;

    @ApiModelProperty(value = "备注")
    @TableId(value ="remark")
    private String remark;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;


}
