package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门用户数据范围表
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysDeptUser对象", description="部门用户数据范围表")
public class SysDeptUser extends SuperEntity {

    @ApiModelProperty(value = "部门用户编号")
    @TableId(value ="dept_id", type = IdType.ASSIGN_UUID)
    private String deptUserId;

    @ApiModelProperty(value = "部门编号")
    @TableId(value ="dept_id")
    private String deptId;

    @ApiModelProperty(value = "用户编号")
    @TableId(value ="user_id")
    private String userId;


}
