package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 岗位表
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysJob对象", description="岗位表")
public class SysJob extends SuperEntity {

    @ApiModelProperty(value = "岗位编号")
    @TableId(value ="job_id", type = IdType.ASSIGN_UUID)
    private String jobId;

    @ApiModelProperty(value = "岗位编码",required = true)
    @NotBlank(message = "岗位编码不能为空！")
    @TableId(value ="job_code")
    private String jobCode;

    @ApiModelProperty(value = "岗位名称",required = true)
    @NotBlank(message = "岗位名称不能为空！")
    @TableId(value ="job_name")
    private String jobName;

    @ApiModelProperty(value = "岗位描述")
    @Length(min = 0, max = 100, message = "岗位描述不能超过100个字符！")
    @TableId(value ="job_desc")
    private String jobDesc;

    @ApiModelProperty(value = "岗位状态（true为启用，false为禁用）",hidden = true)
    @TableId(value ="job_enabled")
    private boolean jobEnabled;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;


}
