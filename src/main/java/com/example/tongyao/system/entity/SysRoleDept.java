package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色部门数据范围表
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysRoleDept对象", description="角色部门数据范围表")
public class SysRoleDept extends SuperEntity {

    @ApiModelProperty(value = "角色部门编号")
    @TableId(value ="role_dept_id", type = IdType.ASSIGN_UUID)
    private String roleDeptId;

    @ApiModelProperty(value = "角色编号")
    @TableId(value ="role_id")
    private String roleId;

    @ApiModelProperty(value = "部门编号")
    @TableId(value ="dept_id")
    private String deptId;


}
