package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门菜单表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysDeptMenu对象", description="部门菜单表")
public class SysDeptMenu extends SuperEntity {

    @ApiModelProperty(value = "部门菜单编号")
    @TableId(value ="dept_menu_id", type = IdType.ASSIGN_UUID)
    private String deptMenuId;

    @ApiModelProperty(value = "部门编号")
    @TableId(value ="dept_id")
    private String deptId;

    @ApiModelProperty(value = "菜单编号")
    @TableId(value ="menu_id")
    private String menuId;


}
