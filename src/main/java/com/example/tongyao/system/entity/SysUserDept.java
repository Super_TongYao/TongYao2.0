package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户部门表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysUserDept对象", description="用户部门表")
public class SysUserDept extends SuperEntity {

    @ApiModelProperty(value = "用户部门编号")
    @TableId(value ="user_dept_id", type = IdType.ASSIGN_UUID)
    private String userDeptId;

    @ApiModelProperty(value = "用户编号")
    @TableId(value ="user_id")
    private String userId;

    @ApiModelProperty(value = "部门编号")
    @TableId(value ="dept_id")
    private String deptId;


}
