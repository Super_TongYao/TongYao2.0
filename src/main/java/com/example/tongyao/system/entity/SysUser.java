package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysUser对象", description="用户表")
public class SysUser extends SuperEntity implements UserDetails {

    @ApiModelProperty(value = "用户编号")
    @TableId(value ="user_id", type = IdType.ASSIGN_UUID)
    private String userId;

    //默认用于转小写：@TableId(value ="committee_code")
    //默认不转原数据：@TableField(value ="Water_Identity_code")

    @ApiModelProperty(value = "用户账号",required = true)
    @TableId(value ="username")
    private String username;

    @ApiModelProperty(value = "用户密码",hidden = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="password")
    private String password;

    @ApiModelProperty(value = "用户昵称",required = true)
    @NotBlank(message = "用户昵称不能为空！")
    @TableId(value ="user_nickname")
    private String userNickname;

    @ApiModelProperty(value = "头像地址")
    @TableId(value ="avatat_address")
    private String avatatAddress;

    @ApiModelProperty(value = "性别（1为男，2为女）",required = true)
    @NotNull(message = "用户性别不能为空！")
    @TableId(value ="sex")
    private int sex;

    @ApiModelProperty(value = "出生日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="birthday_time")
    private Date birthdayTime;

    @ApiModelProperty(value = "电话手机",required = true)
    @NotBlank(message = "电话不能为空！")
    //@Pattern(regexp = "^(\(\d{3,4}-)|\d{3.4}-)?\d{7,8}$")
    @TableId(value ="phone")
    private String phone;

    @ApiModelProperty(value = "邮箱地址")
    @Email(message = "邮箱格式不正确！")
    @Length(min = 0, max = 50, message = "邮箱长度不能超过50个字符！")
    @TableId(value ="email_address")
    private String emailAddress;

    @ApiModelProperty(value = "身份类型（1是前台用户，2是后台用户）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="id_type")
    private int idType;

    @ApiModelProperty(value = "账户状态（true为启用，false为禁用）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="enabled")
    private boolean enabled;

    @ApiModelProperty(value = "锁定状态（true为启用，false为禁用）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="account_non_locked")
    private boolean accountNonLocked;

    @ApiModelProperty(value = "过期状态（true为启用，false为禁用）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="account_non_expired")
    private boolean accountNonExpired;

    @ApiModelProperty(value = "凭证状态（true为启用，false为禁用）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="credentials_non_expired")
    private boolean credentialsNonExpired;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;

    @ApiModelProperty(value = "当前角色的权限",hidden = true)
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String role;


    @ApiModelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleName;

    @ApiModelProperty(value = "角色编号，支持多个以英文逗号拼接",required = true)
    //@NotBlank(message = "角色不能为空！")
    @TableField(exist = false)
    private String roleId;

    @ApiModelProperty(value = "查询到的数据范围",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableField(exist = false)
    private List dataScope;

    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    @ApiModelProperty(value = "部门编号，支持多个以英文逗号拼接",required = true)
    @NotBlank(message = "部门不能为空！")
    @TableField(exist = false)
    private String deptId;

    @ApiModelProperty(value = "岗位名称")
    @TableField(exist = false)
    private String jobName;

    @ApiModelProperty(value = "岗位编号，支持多个以英文逗号拼接",required = true)
    @NotBlank(message = "岗位不能为空！")
    @TableField(exist = false)
    private String jobId;

    @ApiModelProperty(value = "access_token")
    @TableField(exist = false)
    private String access_token;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(role);
    }


    /*@Null	检查该字段为空
    @NotNull	不能为null
    @NotBlank	不能为空，常用于检查空字符串
    @NotEmpty	不能为空，多用于检测list是否size是0
    @Max	该字段的值只能小于或等于该值
    @Min	该字段的值只能大于或等于该值
    @Past	检查该字段的日期是在过去
    @Future	检查该字段的日期是否是属于将来的日期
    @Email	检查是否是一个有效的email地址
    @Pattern(regex=,flag=)	被注释的元素必须符合指定的正则表达式
    @Range(min=,max=,message=)	被注释的元素必须在合适的范围内
    @Size(min=, max=)	检查该字段的size是否在min和max之间，可以是字符串、数组、集合、Map等
    @Length(min=,max=)	检查所属的字段的长度是否在min和max之间,只能用于字符串
    @AssertTrue	用于boolean字段，该字段只能为true
    @AssertFalse	该字段的值只能为false
————————————————
    版权声明：本文为CSDN博主「LifeBackwards」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
    原文链接：https://blog.csdn.net/daodaipsrensheng/article/details/114301602*/

}
