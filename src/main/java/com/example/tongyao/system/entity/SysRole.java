package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysRole对象", description="角色表")
public class SysRole extends SuperEntity {

    @ApiModelProperty(value = "角色编号")
    @TableId(value ="role_id", type = IdType.ASSIGN_UUID)
    private String roleId;

    @ApiModelProperty(value = "角色名称",required = true)
    @NotBlank(message = "角色名称不能为空！")
    @TableId(value ="role_name")
    private String roleName;

    @ApiModelProperty(value = "角色编码",required = true)
    @NotBlank(message = "角色编码不能为空！")
    @TableId(value ="role_code")
    private String roleCode;

    @ApiModelProperty(value = "角色描述")
    @Length(min = 0, max = 100, message = "字典描述不能超过100个字符！")
    @TableId(value ="role_desc")
    private String roleDesc;

    @ApiModelProperty(value = "数据范围",hidden = true)
    @TableId(value ="data_scope")
    private String dataScope;

    @ApiModelProperty(value = "角色状态（true为启用，false为禁用）",hidden = true)
    @TableId(value ="role_enabled")
    private boolean roleEnabled;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;


}
