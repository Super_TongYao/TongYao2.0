package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色菜单表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysRoleMenu对象", description="角色菜单表")
public class SysRoleMenu extends SuperEntity {

    @ApiModelProperty(value = "角色菜单编号")
    @TableId(value ="role_menu_uuid", type = IdType.ASSIGN_UUID)
    private String roleMenuUuid;

    @ApiModelProperty(value = "角色编号")
    @TableId(value ="role_id")
    private String roleId;

    @ApiModelProperty(value = "菜单编号")
    @TableId(value ="menu_id")
    private String menuId;


}
