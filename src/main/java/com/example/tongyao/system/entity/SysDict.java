package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 系统字典表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysDict对象", description="系统字典表")
public class SysDict extends SuperEntity {

    @ApiModelProperty(value = "字典编号")
    @TableId(value ="dict_id", type = IdType.ASSIGN_UUID)
    private String dictId;

    @ApiModelProperty(value = "父级编号")
    @TableId(value ="parent_id")
    private String parentId;

    @ApiModelProperty(value = "字典代码",required = true)
    @NotBlank(message = "字典代码不能为空！")
    @TableId(value ="dict_code")
    private String dictCode;

    @ApiModelProperty(value = "字典名称",required = true)
    @NotBlank(message = "字典名称不能为空！")
    @TableId(value ="dict_name")
    private String dictName;

    @ApiModelProperty(value = "字典描述")
    @Length(min = 0, max = 100, message = "字典描述不能超过100个字符！")
    @TableId(value ="dict_desc")
    private String dictDesc;

    @ApiModelProperty(value = "字典状态（true为启用，false为禁用）",hidden = true)
    @TableId(value ="dict_enabled")
    private boolean dictEnabled;

    @ApiModelProperty(value = "是否是系统内置（1为是，0为否）",required = true)
    @TableId(value ="system_config")
    private Integer systemConfig;

    @ApiModelProperty(value = "字典排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;


}
