package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户岗位表
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysUserJob对象", description="用户岗位表")
public class SysUserJob extends SuperEntity {

    @ApiModelProperty(value = "用户岗位编号")
    @TableId(value ="user_job_id", type = IdType.ASSIGN_UUID)
    private String userJobId;

    @ApiModelProperty(value = "用户编号")
    @TableId(value ="user_id")
    private String userId;

    @ApiModelProperty(value = "岗位编号")
    @TableId(value ="job_id")
    private String jobId;


}
