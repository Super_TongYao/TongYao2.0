package com.example.tongyao.system.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 日志操作表
 * </p>
 *
 * @author tongyao
 * @since 2021-11-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysLog对象", description="日志操作表")
public class SysLog extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "系统日志编号")
    @TableId(value ="log_id", type = IdType.ASSIGN_UUID)
    private String logId;

    @ApiModelProperty(value = "请求方法")
    @TableId(value ="log_method")
    private String logMethod;

    @ApiModelProperty(value = "接口地址")
    @TableId(value ="log_interface")
    private String logInterface;

    @ApiModelProperty(value = "完整接口地址")
    @TableId(value ="full_log_interface")
    private String fullLogInterface;

    @ApiModelProperty(value = "来源IP")
    @TableId(value ="source_ip")
    private String sourceIp;

    @ApiModelProperty(value = "请求参数")
    @TableId(value ="send_data")
    private String sendData;

    @ApiModelProperty(value = "返回结果")
    @TableId(value ="data_result")
    private String dataResult;

    @ApiModelProperty(value = "相应类")
    @TableId(value ="class_pack")
    private String classPack;

    @ApiModelProperty(value = "浏览器")
    @TableId(value ="browser")
    private String browser;

    @ApiModelProperty(value = "浏览器内核")
    @TableId(value ="browser_core")
    private String browserCore;

    @ApiModelProperty(value = "浏览器类型")
    @TableId(value ="browser_type")
    private String browserType;

    @ApiModelProperty(value = "操作系统")
    @TableId(value ="operating_system")
    private String operatingSystem;

    @ApiModelProperty(value = "系统类型")
    @TableId(value ="system_type")
    private String systemType;

    @ApiModelProperty(value = "开始时间")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="start_time")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="end_time")
    private Date endTime;

    @ApiModelProperty(value = "总耗时")
    @TableId(value ="elapsed_ime")
    private String elapsedTime;

    @ApiModelProperty(value = "user-agent")
    @TableId(value ="user_agent")
    private String userAgent;

    @ApiModelProperty(value = "创建者")
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;


}
