package com.example.tongyao.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysUserRole对象", description="用户角色表")
public class SysUserRole extends SuperEntity {

    @ApiModelProperty(value = "用户角色编号")
    @TableId(value ="user_role_id", type = IdType.ASSIGN_UUID)
    private String userRoleId;

    @ApiModelProperty(value = "用户编号")
    @TableId(value ="user_id")
    private String userId;

    @ApiModelProperty(value = "角色编号")
    @TableId(value ="role_id")
    private String roleId;


}
