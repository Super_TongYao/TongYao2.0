package com.example.tongyao.system.entity;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysDept对象", description="部门表")
public class SysDept extends SuperEntity {

    @ApiModelProperty(value = "部门编号")
    @TableId(value ="dept_id", type = IdType.ASSIGN_UUID)
    private String deptId;

    @ApiModelProperty(value = "父级编号",required = true)
    @NotBlank(message = "部门父级不能为空！")
    @TableId(value ="parent_id")
    private String parentId;

    @ApiModelProperty(value = "部门名称",required = true)
    @NotBlank(message = "部门名称不能为空！")
    @TableId(value ="dept_name")
    private String deptName;

    @ApiModelProperty(value = "部门编码",required = true)
    @NotBlank(message = "部门编码不能为空！")
    @TableId(value ="dept_code")
    private String deptCode;

    @ApiModelProperty(value = "部门描述")
    @Length(min = 0, max = 100, message = "部门描述不能超过100个字符！")
    @TableId(value ="dept_desc")
    private String deptDesc;

    @ApiModelProperty(value = "负责人")
    @TableId(value ="leader")
    private String leader;

    @ApiModelProperty(value = "负责人手机")
    @TableId(value ="leader_phone")
    private String leaderPhone;

    @ApiModelProperty(value = "部门状态（true为启用，false为禁用）")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="dept_enabled")
    private boolean deptEnabled;

    @ApiModelProperty(value = "排序")
    @TableId(value ="sort")
    private Integer sort = 1;

    @ApiModelProperty(value = "删除状态（1为未删除，0为已删除）",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="del_flag")
    private Integer delFlag;

    @ApiModelProperty(value = "创建者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="create_by")
    private String createBy;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改者",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value ="modify_by")
    private String modifyBy;

    @ApiModelProperty(value = "修改时间",hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="modify_time")
    private Date modifyTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "子节点",hidden = true)
    private List<SysDept> children;


}
