package com.example.tongyao.system.scheduling.mapper;

import com.example.tongyao.system.scheduling.entity.SysTimedTaskLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
public interface SysTimedTaskLogMapper extends BaseMapper<SysTimedTaskLog> {

}
