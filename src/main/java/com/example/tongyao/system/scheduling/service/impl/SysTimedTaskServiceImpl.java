package com.example.tongyao.system.scheduling.service.impl;

import com.example.tongyao.system.scheduling.entity.SysTimedTask;
import com.example.tongyao.system.scheduling.mapper.SysTimedTaskMapper;
import com.example.tongyao.system.scheduling.service.ISysTimedTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@Service
public class SysTimedTaskServiceImpl extends ServiceImpl<SysTimedTaskMapper, SysTimedTask> implements ISysTimedTaskService {

}
