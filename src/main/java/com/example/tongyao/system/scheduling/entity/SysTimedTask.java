package com.example.tongyao.system.scheduling.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 定时任务表
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysTimedTask对象", description="定时任务表")
public class SysTimedTask extends SuperEntity {

    @ApiModelProperty(value = "主键")
    @TableId(value ="job_id", type = IdType.ASSIGN_UUID)
    private int jobId;

    @ApiModelProperty(value = "任务名称")
    @TableId(value ="job_name")
    private String jobName;

    @ApiModelProperty(value = "所属类名")
    @TableId(value ="class_name")
    private String className;

    @ApiModelProperty(value = "所属方法")
    @TableId(value ="method")
    private String method;

    @ApiModelProperty(value = "参数")
    @TableId(value ="params")
    private String params;

    @ApiModelProperty(value = "定时规则")
    @TableId(value ="cron")
    private String cron;

    @ApiModelProperty(value = "1 开启，0 关闭")
    @TableId(value ="code")
    private int state;


}
