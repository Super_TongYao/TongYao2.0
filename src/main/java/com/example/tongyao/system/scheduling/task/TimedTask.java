package com.example.tongyao.system.scheduling.task;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务方法
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@Log4j2
@Service
public class TimedTask {

    public void timed1() {
        log.info("timed1 定时任务，线程名称："+Thread.currentThread().getName()+"，当前线程ID："+Thread.currentThread().getId());

    }


    public void timed2() {
        log.info("timed2 定时任务，线程名称："+Thread.currentThread().getName()+"，当前线程ID："+Thread.currentThread().getId());

    }

}
