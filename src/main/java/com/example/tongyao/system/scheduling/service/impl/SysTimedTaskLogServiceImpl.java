package com.example.tongyao.system.scheduling.service.impl;

import com.example.tongyao.system.scheduling.entity.SysTimedTaskLog;
import com.example.tongyao.system.scheduling.mapper.SysTimedTaskLogMapper;
import com.example.tongyao.system.scheduling.service.ISysTimedTaskLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@Service
public class SysTimedTaskLogServiceImpl extends ServiceImpl<SysTimedTaskLogMapper, SysTimedTaskLog> implements ISysTimedTaskLogService {

}
