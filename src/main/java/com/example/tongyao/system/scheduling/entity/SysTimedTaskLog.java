package com.example.tongyao.system.scheduling.entity;

import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 定时任务 日志记录表
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysTimedTaskLog对象", description="定时任务日志记录表")
public class SysTimedTaskLog extends SuperEntity {

    @ApiModelProperty(value = "任务名称")
    @TableId(value ="log_id", type = IdType.ASSIGN_UUID)
    private String logId;

    @TableId(value ="job_id")
    private int jobId;

    @TableId(value ="job_name")
    private String jobName;

    @TableId(value ="class_name")
    private String className;

    @TableId(value ="method")
    private String method;

    @TableId(value ="params")
    private String params;

    @TableId(value ="cron")
    private String cron;

    @ApiModelProperty(value = "1自动执行，2手动执行")
    @TableId(value ="exec_type")
    private int execType;

    @ApiModelProperty(value = "0失败，1成功")
    @TableId(value ="exec_status")
    private int execStatus;

    @ApiModelProperty(value = "失败堆栈日志")
    @TableId(value ="exec_msg")
    private String execMsg;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableId(value ="created")
    private Date created;

    @ApiModelProperty(value = "任务名称")
    @TableId(value ="created_by")
    private String createdBy;


}
