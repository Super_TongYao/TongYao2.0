package com.example.tongyao.system.scheduling.service;

import com.example.tongyao.system.scheduling.entity.SysTimedTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 定时任务表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
public interface ISysTimedTaskService extends IService<SysTimedTask> {

}
