package com.example.tongyao.system.scheduling.controller;

import com.example.tongyao.system.scheduling.config.TimedTaskConfig;
import com.example.tongyao.system.scheduling.entity.SysTimedTask;
import com.example.tongyao.utils.DataResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 定时任务表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@RestController
@Api(tags = "系统_定时规则刷新规则")
@RequestMapping("/system/sys-timed-task")
public class SysTimedTaskController {

    @Autowired
    private TimedTaskConfig timedTaskConfig;

    /**
     * 刷新定时任务配置生效
     * @return
     */
    @ApiOperation(
            value = "刷新定时规则",
            notes = "刷新定时任务配置规则",
            httpMethod = "GET")
    @GetMapping(value = "/refresh" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult refreshTask() {
        timedTaskConfig.refreshAll();
        return DataResult.setSuccess(null);
    }

    /**
     * 手动执行任务
     * @param job
     * @return
     * @throws ClassNotFoundException
     */
    @ApiOperation(
            value = "手动执行任务",
            notes = "手动执行任务",
            httpMethod = "GET")
    @GetMapping("/execTask")
    public DataResult execTask(SysTimedTask job) throws ClassNotFoundException {
        Assert.notNull(job.getClassName(),"ClassName为空");
        Assert.notNull(job.getMethod(),"Method为空");
        timedTaskConfig.execTask(job);
        return DataResult.setSuccess(null);
    }
}
