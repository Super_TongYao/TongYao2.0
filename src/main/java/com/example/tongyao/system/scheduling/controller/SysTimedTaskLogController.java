package com.example.tongyao.system.scheduling.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@RestController
@RequestMapping("/system/sys-timed-task-log")
public class SysTimedTaskLogController {

}
