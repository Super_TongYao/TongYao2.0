package com.example.tongyao.system.scheduling.service;

import com.example.tongyao.system.scheduling.entity.SysTimedTaskLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
public interface ISysTimedTaskLogService extends IService<SysTimedTaskLog> {

}
