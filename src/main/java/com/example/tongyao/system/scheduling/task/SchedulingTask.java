package com.example.tongyao.system.scheduling.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

/**
 * <p>
 * 定时器
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class SchedulingTask {


    //单线程 注解定时任务
    /*@Scheduled(cron = "0/1 * * * * ?")
    private void configureTasks2() {
        System.err.println("执行静态定时任务时间2: " + LocalDateTime.now());
    }*/


}
