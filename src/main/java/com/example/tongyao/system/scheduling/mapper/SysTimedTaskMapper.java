package com.example.tongyao.system.scheduling.mapper;

import com.example.tongyao.system.scheduling.entity.SysTimedTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2022-01-21
 */
public interface SysTimedTaskMapper extends BaseMapper<SysTimedTask> {

}
