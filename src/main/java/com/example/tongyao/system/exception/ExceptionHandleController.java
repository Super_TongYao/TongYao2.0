package com.example.tongyao.system.exception;
import com.example.tongyao.utils.DataResult;
import javassist.NotFoundException;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 统一异常处理类
 * 捕获程序所有异常，针对不同异常，采取不同的处理方式
 *
 * @version 2.0
 * @author tongyao
 * @since 2021-08-01
 */
@ControllerAdvice
@RestController
public class ExceptionHandleController {

    //没有抛出异常，所以错误日志中不会记录
    @ExceptionHandler(value = Exception.class)
    public DataResult handle(Exception e) {
        e.printStackTrace();
        if(e instanceof AccessDeniedException){
            //401 没有权限
            return DataResult.setResult(HttpStatus.SC_UNAUTHORIZED,e.getMessage());
        }else if(e instanceof NoHandlerFoundException){
            //404 未找到
            return DataResult.setResult(HttpStatus.SC_NOT_FOUND, ((NoHandlerFoundException) e).getRequestURL()+
            "（"+((NoHandlerFoundException) e).getHttpMethod()+"）接口没有找到！！");
        }else if(e instanceof HttpRequestMethodNotSupportedException){
            //500 请求格式类型错误
            return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,
                    "（"+((HttpRequestMethodNotSupportedException) e).getMethod()+"）类型请求错误！！应该为:"
                            +((HttpRequestMethodNotSupportedException) e).getSupportedHttpMethods());
        }else if(e instanceof BadSqlGrammarException){
            //500 SQL异常
            return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,"SQL出现异常："+e.getMessage());
        }else if(e instanceof HttpMessageNotReadableException){
            //500 json对象为空
            return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,"json对象为空！"+e.getMessage());
        }else if(e instanceof MethodArgumentNotValidException){
            //500 字段为空异常
            BindingResult bindingResult = ((MethodArgumentNotValidException) e).getBindingResult();
            FieldError fieldError = bindingResult.getFieldError();
            return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,fieldError.getDefaultMessage());
        }else if(e instanceof RuntimeException){
            //500 运行时异常
            return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,e.getMessage());
        }else if(e instanceof MissingServletRequestParameterException){
            //500 相应必要参数没传入
            return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,"请传入相应必要参数："+
                    ((MissingServletRequestParameterException) e).getParameterName());
        }
        return DataResult.setError("未知异常，请联系管理员！"+e.getMessage());
    }


}
