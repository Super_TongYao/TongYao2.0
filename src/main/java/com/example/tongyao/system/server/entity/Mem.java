package com.example.tongyao.system.server.entity;

import com.example.tongyao.utils.tongyao.TyDoubleUtils;
import lombok.Setter;

/**
 * 內存相关信息
 *
 * @author tongyao
 * @since 2022-01-20 17:15
 */
@Setter
public class Mem {

    //内存总量
    private double total;

    //已用内存
    private double used;

    //剩余内存
    private double free;


    public double getTotal(){
        return TyDoubleUtils.div(total, (1024 * 1024 * 1024), 2);
    }

    public double getUsed() {
        return TyDoubleUtils.div(used, (1024 * 1024 * 1024), 2);
    }

    public double getFree() {
        return TyDoubleUtils.div(free, (1024 * 1024 * 1024), 2);
    }

    public double getUsage() {
        return TyDoubleUtils.mul(TyDoubleUtils.div(used, total, 4), 100);
    }
}
