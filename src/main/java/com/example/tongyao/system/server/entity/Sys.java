package com.example.tongyao.system.server.entity;

import lombok.Setter;

/**
 * 系统相关信息
 *
 * @author tongyao
 * @since 2022-01-20 17:15
 */
@Setter
public class Sys {

    //服务器名
    private String computerName;

    //服务器Ip
    private String computerIp;

    //项目路径
    private String userDir;

    //操作系统
    private String osName;

    //系统架构
    private String osArch;

    public String getComputerName() {
        return computerName;
    }

    public String getComputerIp() {
        return computerIp;
    }

    public String getUserDir() {
        return userDir;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsArch() {
        return osArch;
    }
}
