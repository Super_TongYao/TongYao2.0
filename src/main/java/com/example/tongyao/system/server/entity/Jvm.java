package com.example.tongyao.system.server.entity;

import com.example.tongyao.utils.DateUtils;
import com.example.tongyao.utils.tongyao.TyDoubleUtils;
import lombok.Setter;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * JVM相关信息
 *
 * @author tongyao
 * @since 2022-01-20 17:05
 */
@Setter
public class Jvm {

    //当前JVM占用的内存数(M)
    private double total;

    //JVM大可用内存数(M)
    private double max;

    //JVM空闲内存(M)
    private double free;

    //JDK版本
    private String version;

    //JDK路径
    private String home;


    public double getTotal(){
        return TyDoubleUtils.div(total, (1024 * 1024), 2);
    }


    public double getMax(){
        return TyDoubleUtils.div(max, (1024 * 1024), 2);
    }


    public double getFree(){
        return TyDoubleUtils.div(free, (1024 * 1024), 2);
    }


    public double getUsed(){
        return TyDoubleUtils.div(total - free, (1024 * 1024), 2);
    }

    public double getUsage(){
        return TyDoubleUtils.mul(TyDoubleUtils.div(total - free, total, 4), 100);
    }

    /**
     * 获取JDK名称
     */
    public String getName(){
        return ManagementFactory.getRuntimeMXBean().getVmName();
    }

    public String getVersion(){
        return version;
    }


    public String getHome(){
        return home;
    }


    /**
     * JDK启动时间
     */
    public String getStartTime(){
        return new SimpleDateFormat(
                DateUtils.YYYY_MM_DD_HH_MM_SS).format(DateUtils.getServerStartDate());
    }

    /**
     * JDK运行时间
     */
    public String getRunTime(){
        return DateUtils.getDatePoor(new Date(), DateUtils.getServerStartDate());
    }
}
