package com.example.tongyao.system.server.controller;

import com.example.tongyao.TongyaoApplication;
import com.example.tongyao.system.server.entity.ServerInfo;
import com.example.tongyao.utils.DataResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.SpringApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 操作系统 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2022-01-20 16:37
 */
@Api(tags = "TongYao框架默认操作系统控制器")
@RestController
@RequestMapping("/system")
public class SystemController {

    @ApiOperation(
            value = "查询服务器信息",
            notes = "查询服务器当前CPU、内存、存储等情况信息。",
            httpMethod = "GET")
    @GetMapping(value = "/server", produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult server() throws Exception {
        //获取系统详细信息：https://cloud.tencent.com/developer/article/1665800
        return DataResult.setSuccess(new ServerInfo());
    }

    @ApiOperation(
            value = "重启应用服务器",
            notes = "重启应用服务器",
            httpMethod = "GET")
    @GetMapping(value = "/restart", produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult restart() {
        System.exit(1);
        return DataResult.setSuccess(null);
    }





}
