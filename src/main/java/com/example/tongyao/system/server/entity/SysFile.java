package com.example.tongyao.system.server.entity;

import lombok.Data;

/**
 * 系统文件相关信息
 *
 * @author tongyao
 * @since 2022-01-20 17:15
 */
@Data
public class SysFile {
    
    //盘符路径
    private String dirName;
    
    //盘符类型
    private String sysTypeName;
    
    //文件类型
    private String typeName;

    //总大
    private String total;

    //剩余大小
    private String free;

    //已经使用
    private String used;

    //资源的使用率
    private double usage;

}
