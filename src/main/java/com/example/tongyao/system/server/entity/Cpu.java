package com.example.tongyao.system.server.entity;

import com.example.tongyao.utils.tongyao.TyDoubleUtils;
import lombok.Setter;

/**
 * CPU相关信息
 *
 * @author tongyao
 * @since 2022-01-20 17:05
 */
@Setter
public class Cpu {

    //核心
    private int cpuNum;

    //CPU总的使用
    private double total;

    //CPU系统使用
    private double sys;

    //CPU用户使用
    private double used;

    //CPU当前等待
    private double wait;

    //CPU当前空闲
    private double free;

    public int getCpuNum(){
        return cpuNum;
    }

    public double getTotal(){
        return TyDoubleUtils.round(TyDoubleUtils.mul(total, 100), 2);
    }

    public double getSys(){
        return TyDoubleUtils.round(TyDoubleUtils.mul(sys / total, 100), 2);
    }

    public double getUsed(){
        return TyDoubleUtils.round(TyDoubleUtils.mul(used / total, 100), 2);
    }

    public double getWait(){
        return TyDoubleUtils.round(TyDoubleUtils.mul(wait / total, 100), 2);
    }

    public double getFree(){
        return TyDoubleUtils.round(TyDoubleUtils.mul(free / total, 100), 2);
    }

}
