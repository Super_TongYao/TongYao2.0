package com.example.tongyao.system.config;


import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.utils.token.UserUtils;


public class DataScopeConfig {

    String dataScope = "";

    public String getDataScope(){

        SysUser user = (SysUser) UserUtils.getUserInfo();
        if(user.getDataScope().indexOf("sys_all_data") == -1){

            //仅本人数据
            if(user.getDataScope().indexOf("sys_self_data") == -1){
                dataScope = " and create_by = " + user.getUserId();
            }

            //本部门数据
            if(user.getDataScope().indexOf("sys_dept_data") == -1){

            }

            //本部门及以下数据
            if(user.getDataScope().indexOf("sys_dept_children_data") == -1){

            }

            //指定部门数据
            if(user.getDataScope().indexOf("sys_specified_dept_data") == -1){

            }
        }else{
            dataScope = " and 1=0";
        }

        return dataScope;
    }

}
