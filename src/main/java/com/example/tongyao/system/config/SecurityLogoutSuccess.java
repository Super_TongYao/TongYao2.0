package com.example.tongyao.system.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TongYao2.0 >>> 【com.example.tongyao.system.config】
 * Security 自定义退出
 *
 * @author: tongyao
 * @since: 2021-08-01 10:53
 */
public class SecurityLogoutSuccess implements LogoutSuccessHandler {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 退出成功 跳到那个接口 上
     *
     * @param request
     * @param response
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        new DefaultRedirectStrategy().sendRedirect(request, response, "/logoutInfo");
    }


}