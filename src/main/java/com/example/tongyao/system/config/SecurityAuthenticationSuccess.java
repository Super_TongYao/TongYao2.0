package com.example.tongyao.system.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TongYao2.0 >>> 【com.example.tongyao.system.config】
 * Security 处理登录校验成功的
 *
 * @author: tongyao
 * @since: 2021-08-01 11:16
 */
@Component
public class SecurityAuthenticationSuccess extends SavedRequestAwareAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        new DefaultRedirectStrategy().sendRedirect(request, response, "/getUser");

    }
}