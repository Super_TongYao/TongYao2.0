package com.example.tongyao.system.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * tongyao >>> 【com.example.tongyao.system.config】
 * MyBatisPlus分页配置
 *
 * @author: tongyao
 * @since: 2020-06-14 17:15
 */
@Configuration
public class MybatisPlusConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor page = new PaginationInterceptor();
        //page.setDialectType("mysql");
        return page;
    }
}