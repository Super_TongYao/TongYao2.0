package com.example.tongyao.system.message;

public class SystemMessage {

    public static final String user = "用户";

    public static final String dept = "部门";
    public static final String role = "角色";

    public static final String menu = "菜单";
    public static final String job = "岗位";

    public static final String dict = "字典";

    public static final String config = "配置";

    public static final String freeAuthPath = "免认证接口";

    public static final String relation = "关系";
    public static final String info = "信息";

    public static final String state = "状态";
    public static final String dataScope = "数据范围";
    public static final String allot = "分配";
    public static final String sysInterface = "接口";
    public static final String passWord = "密码";

    public static final String query = "查询";
    public static final String get = "获取";
    public static final String delete = "删除";
    public static final String add = "添加";
    public static final String update = "更新";


    public static final String insert = "插入";

    public static final String save = "保存";

    public static final String fail = "失败";

    public static final String success = "成功";

    public static final String a = "！";
    public static final String b = "？";
}
