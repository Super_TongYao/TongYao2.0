package com.example.tongyao.system.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 通用接口
 */
public interface CommonMapper {

    //查询
    public Map<String,Object> getOne(
            @Param("field") String field,
            @Param("table") String table,
            @Param("where") String where
    );

    //查询
    public List<Map<String,Object>> getMore(
            @Param("field") String field,
            @Param("table") String table,
            @Param("where") String where
    );

    //数量
    public int getCount(
            @Param("table") String table,
            @Param("where") String where
    );
}
