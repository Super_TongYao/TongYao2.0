package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysRoleAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 角色接口 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysRoleAuthMapper extends BaseMapper<SysRoleAuth> {

    public List<String> getAuthMenuInterfaceAddressByRoleId(String roleId);

}
