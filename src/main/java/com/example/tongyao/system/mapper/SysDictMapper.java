package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统字典表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
