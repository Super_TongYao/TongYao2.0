package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysFreeAuth;
import com.example.tongyao.system.entity.SysInterface;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单接口表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysInterfaceMapper extends BaseMapper<SysInterface> {

    List<SysInterface> interfacePage(Map<String, Object> params);

}
