package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

    SysConfig getConfigByKey(String configKey);
}
