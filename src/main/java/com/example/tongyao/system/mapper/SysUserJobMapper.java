package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysUserJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 用户岗位表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface SysUserJobMapper extends BaseMapper<SysUserJob> {

    Map<String,Object> getUserJobInfoByUserId(String userId);
}
