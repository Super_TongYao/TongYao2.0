package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysErrorLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日志操作表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-11-16
 */
public interface SysErrorLogMapper extends BaseMapper<SysErrorLog> {

}
