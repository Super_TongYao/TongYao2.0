package com.example.tongyao.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.tongyao.system.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-15
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /*List<SysUser> userPage(Map<String, Object> params);
    int userPageCount(Map<String, Object> params);*/

    //3代 自定义分页查询数据
    IPage<SysUser> userPage(@Param("page") Page<Map<String,Object>> page, @Param("params") Map<String, Object> params);


    boolean deleteUserByUserId(String userId);

    boolean resetPasswordByUserId(
            @Param("userId") String userId,
            @Param("password") String password
    );
}
