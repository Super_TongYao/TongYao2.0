package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    String getMenuInfoByRoleIdDeptId(
            @Param("roleId") String roleId,
            @Param("deptId") String deptId
    );
}
