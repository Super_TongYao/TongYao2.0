package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tongyao.system.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    List<SysDept> deptPage(Map<String, Object> params);
}
