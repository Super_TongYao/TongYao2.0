package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.system.entity.SysUserDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户部门表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysUserDeptMapper extends BaseMapper<SysUserDept> {

    Map<String,Object> getUserDeptInfoByUserId(String userId);


}
