package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysRoleDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色部门数据范围表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
