package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    Map<String,Object> getUserRoleInfoByUserId(String userId);
}
