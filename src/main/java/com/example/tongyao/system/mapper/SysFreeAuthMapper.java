package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysFreeAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tongyao.system.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限路径表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysFreeAuthMapper extends BaseMapper<SysFreeAuth> {

    List<SysFreeAuth> freeAuthPage(Map<String, Object> params);
}
