package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysDeptUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门用户数据范围表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-09-05
 */
public interface SysDeptUserMapper extends BaseMapper<SysDeptUser> {

}
