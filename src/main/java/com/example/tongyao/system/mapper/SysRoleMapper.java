package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    public List<SysMenu> getRoleInterfaceByRoleId(String roleId);
}
