package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日志操作表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-11-07
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
