package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysDeptMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门菜单表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-08-18
 */
public interface SysDeptMenuMapper extends BaseMapper<SysDeptMenu> {

}
