package com.example.tongyao;

import com.example.tongyao.utils.IpUtils;
import lombok.extern.log4j.Log4j2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;

/**
 * TongYao 2.0版本启动类
 *
 * @author tongyao
 * @since 2021-08-01 10点15分
 */
@SpringBootApplication
@EnableTransactionManagement //事务控制器
@MapperScan(basePackages = {
        "com.example.tongyao.system.mapper",
        "com.example.tongyao.system.scheduling.mapper",
        "com.example.tongyao.core.mapper"})
@Log4j2
public class TongyaoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception{
        ConfigurableApplicationContext application = SpringApplication.run(TongyaoApplication.class, args);

        Environment env = application.getEnvironment();
        String applicationName = env.getProperty("spring.application.name");
        String ipPath = IpUtils.getLocalHostLANAddress().getHostAddress();
        String port = env.getProperty("server.port");
        log.info("\n----------------------------------------------------------\n" +
                        "Application '{}' is running! Access URLs:\n" +
                        "doc: http://{}:{}/doc.html\n" +
                        "swagger: http://{}:{}/swagger-ui.html\n" +
                        "druid: http://{}:{}/druid/login.html\n",
                applicationName,
                ipPath,port,
                ipPath,port,
                ipPath,port);
    }

    //打war包启动口
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TongyaoApplication.class);
    }

}
