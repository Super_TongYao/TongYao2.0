package com.example.tongyao.utils.token;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * TongYao2.0 >>> 【com.example.tongyao.system.utils】
 * 用户工具类
 *
 * @author: tongyao
 * @since: 2021-08-04 13:07
 */
public class UserUtils {
    /**
     * 获取用户对象信息
     * @return
     */
    public static Object getUserInfo(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object obj = authentication.getPrincipal();
        if(obj.equals("anonymousUser")){
            obj = "免登录接口无用户信息！";
        }
        return obj;
    }

    /**
     * 获取用户名
     * @return
     */
    public static String getUserName(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        if(userName.equals("anonymousUser")){
            userName = "免登录接口无用户信息！";
        }
        return userName;
    }


}
