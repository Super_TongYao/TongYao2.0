package com.example.tongyao.utils.generator;

import java.util.Random;
import java.util.UUID;

/**
 * UUID生成工具
 *
 * @version 1.0
 * @author tongyao
 * @since 2020-06-13
 */
public class GeneratorUUID {

    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        String temp = str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) + str.substring(19, 23) + str.substring(24);
        return str + "," + temp;
    }

    //获得指定数量的UUID
    public static String[] getUUID(int number) {
        if (number < 1) {
            return null;
        }
        String[] ss = new String[number];
        for (int i = 0; i < number; i++) {
            ss[i] = getUUID();
        }
        return ss;
    }


    public static String getOrderIdByUUId() {
        int machineId = 1;//最大支持1-9个集群机器部署
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {//有可能是负数
            hashCodeV = -hashCodeV;
        }
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        return machineId + String.format("%031d", hashCodeV);
    }

    public static String uuid() {
        String result = new String();
        for (int j = 0; j < 1; j++) {
            char[] para = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

            result += "WS";
            for (int i = 0; i < 30; i++) {
                int c = new Random().nextInt(35);
                result += String.valueOf(para[c]);
            }
            return result;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(uuid());
        System.out.println(getUUID());
        System.out.println(getOrderIdByUUId());
    }
}
