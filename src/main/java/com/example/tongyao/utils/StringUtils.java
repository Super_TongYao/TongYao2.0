package com.example.tongyao.utils;

/**
 * TongYao2.0 >>> 【com.example.tongyao.utils】
 * String工具类
 *
 * @author: tongyao
 * @since: 2021-09-21 13:05
 */
public class StringUtils {

    public static boolean isBlank(String str){
        return str == null || "".equals(str.trim()) ?true:false;
    }

    public static boolean isNotBlank(String str){
        return str!=null&&!"".equals(str.trim())&&!"null".equals(str)?true:false;
    }

    public static boolean isNull(Object str){
        return str == null?true:false;
    }

    public static boolean isNotNull(Object str){
        return str != null?true:false;
    }

}