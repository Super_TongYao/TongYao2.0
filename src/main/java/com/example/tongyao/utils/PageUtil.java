package com.example.tongyao.utils;
import com.example.tongyao.common.SuperEntity;
import com.example.tongyao.system.config.DataScopeConfig;
import com.example.tongyao.system.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysql.jdbc.Util;
import com.sun.mail.imap.protocol.Item;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TongYao2.0 >>> 【com.example.tongyao.utils】
 * 自定义查询分页工具
 *
 * @author: tongyao
 * @since: 2021-08-27 21:03
 */
@Data
public class PageUtil<T> {

    private static final long serialVersionUID = 1L;

    /**
     * 返回的记录集合
     */
    private List<T> records;

    /**
     * 总记录条数
     */
    private long total;

    /**
     * 每页记录数
     */
    private int size;

    /**
     * 分页起始页
     */
    private int current;

    private String sortBy;

    private String order;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    DataScopeConfig dataScopeConfig = new DataScopeConfig();

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String dataScope = "";

    public PageUtil(int current,int size){
        if(current < 1){
            current = 1;
        }
        this.current = (current-1)*size;
        this.size = size;

        dataScope = dataScopeConfig.getDataScope();
    }

    public PageUtil(int current,int size,String sortBy,String order){
        if(current < 1){
            current = 1;
        }
        this.current = (current-1)*size;
        this.size = size;
        this.sortBy = sortBy;
        this.order = order;

        dataScope = dataScopeConfig.getDataScope();
    }

    public void setPage(List<T> records,int total,int current){
        this.records = records;
        this.total = total;
        this.current = current;
    }

}