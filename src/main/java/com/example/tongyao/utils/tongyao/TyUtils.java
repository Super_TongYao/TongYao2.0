package com.example.tongyao.utils.tongyao;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * TongYao2.0 >>> 【com.example.tongyao.utils】
 * TongYao工具类
 *
 * @author: tongyao
 * @since: 2021-08-28 11:09
 */
public class TyUtils {

    /*public Map<Object,Object> toObjectMap(Object o){
        return ;
    }*/


    public static Map<String, Object> toObjectMap(Object obj) {
        String jsonStr = JSONObject.toJSONString(obj);
        return JSONObject.parseObject(jsonStr);
    }

    /**
     * 字节转换
     *
     * @param size 字节大小
     * @return 转换后
     */
    public static String convertFileSize(long size){

        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;
        if (size >= gb)
        {
            return String.format("%.1f GB", (float) size / gb);
        }
        else if (size >= mb)
        {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        }
        else if (size >= kb)
        {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        }
        else
        {
            return String.format("%d B", size);
        }
    }


}