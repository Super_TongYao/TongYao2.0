package com.example.tongyao.utils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TongYao2.0 >>> 【com.example.tongyao.utils】
 * TongYao日期类
 *
 * @author: tongyao
 * @since: 2021-09-09 21:47
 */
public class DateUtils {

    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     * 获得当前系统的年月日秒数
     *
     * @return
     */
    public static Date getDateTime(){
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        try {
            date = simpleDateFormat.parse(simpleDateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void main(String[] args) {
        System.out.println((java.sql.Date) getDateTime());
    }

    public static String calculateHMS(long time) {

        String str = "0." + time + "秒";
        long day = time / (24 * 60 * 60 * 1000);
        if (day > 0) {
            str = day + "天";
        }
        long hours = (time / (60 * 60 * 1000) - day * 24);
        if (hours > 0) {
            str = hours + "时";
        }
        long minutes = ((time / (60 * 1000)) - day * 24 * 60 - hours * 60);
        if (minutes > 0) {
            str = minutes + "分";
        }
        long seconds = (time / 1000 - day * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60);
        if (seconds > 0) {
            str = seconds + "秒";
        }
        return str;
    }


    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate(){
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate){
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        long diff = endDate.getTime() - nowDate.getTime();
        long day = diff / nd;
        long hour = diff % nd / nh;
        long min = diff % nd % nh / nm;
        return day + "天" + hour + "小时" + min + "分钟";
    }
}