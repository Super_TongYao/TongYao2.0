package com.example.tongyao.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.http.HttpStatus;

/**
 * 数据结果返回类
 *
 * @version 2.0
 * @author tongyao
 * @since 2021-08-01
 */
@Data
@ApiModel(value="返回结果处理类", description="结果返回类")
public class DataResult {

    @ApiModelProperty(value = "状态码")
    private int code = HttpStatus.SC_OK;

    @ApiModelProperty(value = "消息结果")
    private String message = "success";

    @ApiModelProperty(value = "数据结果")
    private Object data = null;

    public DataResult() {

    }

    public DataResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public DataResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 设置状态为200的返回结果数据
     * @param data
     * @return
     */
    public static DataResult setResult(Object data) {
    	DataResult dataResult = new DataResult();
    	dataResult.setData(data);
    	return dataResult;
    }

    /**
     * 设置自定义状态的返回结果数据
     * @param data
     * @return
     */
    public static DataResult setResult(int code, Object data) {
    	DataResult dataResult = new DataResult();
    	dataResult.setCode(code);
    	dataResult.setData(data);
    	return dataResult;
    }

    /**
     * 设置自定义状态和返回信息返回结果数据
     * @param data
     * @return
     */
    public static DataResult setResult(int code, String message, Object data) {
    	DataResult dataResult = new DataResult();
    	dataResult.setCode(code);
    	dataResult.setMessage(message);
    	dataResult.setData(data);
    	return dataResult;
    }

    /**
     * 设置自定义状态和返回信息内容，但无结果数据
     * @param code
     * @param message
     * @return
     */
    public static DataResult setResult(int code, String message) {
    	DataResult dataResult = new DataResult();
    	dataResult.setCode(code);
    	dataResult.setMessage(message);
    	return dataResult;
    }

    /**
     * 设置默认200状态，自定义返回信息和结果数据
     * @param data
     * @return
     */
    public static DataResult setResult(String message, Object data) {
    	DataResult dataResult = new DataResult();
    	dataResult.setMessage(message);
    	dataResult.setData(data);
    	return dataResult;
    }

    /**
     * 默认返回错误方法，自定义返回错误信息
     * @param message
     * @return
     */
    public static DataResult setError(String message) {
        DataResult dataResult = new DataResult();
        dataResult.setCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        dataResult.setMessage(message);
        return dataResult;
    }

    /**
     * 默认返回成功方法，自定义返回结果数据
     * @param data
     * @return
     */
    public static DataResult setSuccess(Object data) {
        DataResult dataResult = new DataResult();
        dataResult.setData(data);
        return dataResult;
    }
    
}
