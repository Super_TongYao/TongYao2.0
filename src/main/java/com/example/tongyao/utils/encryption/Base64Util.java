package com.example.tongyao.utils.encryption;


/**
 * TongYao2.0 >>> 【com.example.tongyao.utils】
 * Base64加解密算法
 *
 * @author: tongyao
 * @since: 2021-08-27 21:03
 */
public class Base64Util {

    /**
     * 加密
     * @param text
     * @return
     */
    public static String encryption(String text){
        return java.util.Base64.getEncoder().encodeToString(text.getBytes());
    }

    /**
     * 解密
     * @param text
     * @return
     */
    public static String decryption(String text){
        return new String(java.util.Base64.getDecoder().decode(text));
    }


    public static void main(String[] args) {
        System.out.println(encryption("123456"));
        System.out.println(decryption("MTIzNDU2"));
    }
}
