<p align="center"></p>

<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">TongYao v2.0</h1>
<p align="center">基于SpringBoot打造开箱即用轻量级框架</p>

<p align="center">
	<img src="https://img.shields.io/badge/TongYao-v2.0-green.svg" alt="MySQL">
	<a href="https://gitee.com/Super_TongYao/TongYao2.0/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
        <a href="https://blog.csdn.net/u014641168/article/details/121945400"><img src="https://img.shields.io/badge/Restful-协议-green.svg" alt="Restful"></a>
</p>

### 框架描述
本开源框架目前为纯后端框架，适用开发公司企业管理系统。前后台分离结构，后台接口采用Restful协议，内置用户管理、角色管理、菜单管理、部门管理、岗位管理、接口管理、接口监控、角色权限管理、角色鉴权管理、代码生成器等等核心系统业务，在安全上又基于 Token 令牌又做了接口日志监控、对外接口鉴权，完全可以暴露接口给第三方对接，可以适配 Vue、微信小程序、H5、Android 等多种前端。本开源框架内部集成Spring Boot、Jwt、Security、MyBatisPlus、Swagger 打造的轻量级框架，数据库采用大众MySQl5.7版本，前期版本为TongYao框架后又重新改造后升级为 TongYao2.0 版本，完全诠释了开箱即用，直接开干。

<p align="center"> ** 欢迎Star和Fork！ **   :stuck_out_tongue: </p> 

### 在线演示

- druid监控：http://82.157.190.245:8088/druid
（admin/admin）
- 服务器信息监控：http://82.157.190.245:8088/system
- 系统接口文档演示地址：http://82.157.190.245:8088/doc.html

### 文档教程
https://gitee.com/Super_TongYao/TongYao2.0/wikis/pages

### 内置功能

1. 用户管理：用于对本系统所有用户进行添加、删除、更新、账号过期、锁定用户等操作。
1. 角色管理：根据业务需求给每个用户分配不同角色操作不同业务数据。
1. 菜单管理：管理本系统菜单项做CRUD。
1. 岗位管理：根据业务需求，针对用户所在单位机构进行管理。
1. 部门管理：针对核心业务需求，管理不同单位机构人员。
1. 字典管理：针对核心业务的一个数据集。
1. 接口监控：用于Aop实现接口监控，更清晰明了查看接口的请求时的IP、目标接口、提交Json、请求时间、返回数据结果和当前操作系统版本号和浏览器版本号。
1. 代码生成：输入代码包名和要生成的数据库表一键生成，后台代码会多出这些数据表的实体、mapper、service、controller。
1. 接口管理：用于CRUD管理本系统全部数据接口。
1. 权限校验：针对实际业务需求给用户分配权限菜单。
1. 接口鉴权：用于接口鉴权，解决菜单地址泄露还可以操作接口问题。
1. 系统配置：主要用于对系统一些较小参数的配置，如是否开启Token加密、是否开启短信发送等。
1. SQL监控：连接数据库连接池，监控各个SQL状态和SQL的运行效率。
1. 服务器信息查询：查询当前服务器运行状态。
1. 全局异常捕获：全局代码异常捕获，统一管理。
1. 免登录接口管理：不需要登录就可以访问的接口管理。
1. 免权限认证接口管理：不需要权限鉴权就可以访问的接口管理。
1. 角色/部门菜单接口授权：用于授权角色、菜单等一些接口和菜单的权限。

### 技术总栈
<!-- <a href="http://spring.io/projects/spring-boot"><img src="https://img.shields.io/badge/Spring--Boot-v2.2.6-green.svg" alt="spring-boot"></a>
<a href="https://jwt.io/introductiont"><img src="https://img.shields.io/badge/jwt-v3.8.2-green.svg" alt="jwt"></a>
<a href="http://spring.io/projects/spring-security"><img src="https://img.shields.io/badge/spring--security-v2.2.6-green.svg" alt="spring-security"></a>
<img src="https://img.shields.io/badge/MyBatis--Plus-v3.1.1-green.svg" alt="MyBatis-Plus">
<br>
<a href="http://spring.io/projects/swaager"><img src="https://img.shields.io/badge/swaager-v2.8.0-green.svg" alt="swaager"></a>
<img src="https://img.shields.io/badge/MySQL-v5.7-green.svg" alt="MySQL">

 - SpringBoot v2.2.6
- SpringSecurity v2.2.6
- Jwt v3.8.2 -->


- SpringBoot-v2.2.6
- Security-v2.2.6
- Jwt-v3.8.2
- MyBatisPlus-v3.1.1
- Swaager-v2.8.0
- MySQL-v5.7
- Druid-v1.1.10
- Lombok-v1.18.8
- Fastjson-v1.2.41
- Kaptcha-v2.3.2
- Httpclient-v4.5.12
- UserAgentUtils-v1.20
- Aop-v2.2.6
- Oshi-core-v3.9.1